withDate:false,			//是否带日期
withWeek:false,			//是否带星期
timeNotation: '24h',	//24小时制
am_pm: false,			//是否显示上下午，好像看不出来	
utc: false,				//UTC世界标准时间
fontFamily: '',			//字体设置
fontSize: '',			//字体大小设置
foreground: '',			//字体颜色设置
background: ''			//时间背景设置