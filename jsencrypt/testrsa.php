<?php
require_once('test.php');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
$key = array();
$act = $_REQUEST['act'];
$rsa = new Rsa();
session_start();
if ( $act == 'get' ) {
    $key = $rsa->getKeys();
    $arr = array('code'=>200, 'publicKey'=>$key['publicKey']);
    // echo $key['publicKey'];
    echo json_encode($arr);
    $_SESSION['publickey']  = $key['publicKey'];
    $_SESSION['privateKey'] = $key['privateKey'];
} else if ( $act == 'data' ) {
    $dataText = $_REQUEST['dataText'];
    $decrypted = $rsa->decrypt($dataText, $_SESSION['privateKey']);
    $arr = array('code'=>200, 'decrypted'=>$decrypted);
    echo json_encode($arr);
}
